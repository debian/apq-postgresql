#!/usr/bin/make -f

PN = apq-postgresql
LIBDIR = debian/tmp/usr/lib/$(PN)/relocatable

include /usr/share/dpkg/buildflags.mk
include /usr/share/ada/debian_packaging.mk

# Explicit list of exported variables.
# dh_auto_build will not erase existing values, and ignores ADAFLAGS.
export CFLAGS
export CPPFLAGS
export LDFLAGS
export ADAFLAGS

SOVERSION=3.2.0.1
export SOVERSION

%:
	dh ${@}

override_dh_auto_clean:
	$(MAKE) distclean

	# Restore project file to its original state
	-mv -f debian/$(PN).gpr.in gnat/$(PN).gpr.in
	rm -f samples/$(PN)-examples.gpr
	rm -f *.cgpr

override_dh_auto_configure:
	# Save project file so we can restore it on clean
	cp -f gnat/$(PN).gpr.in debian/$(PN).gpr.in
	# -j in BUILDER_OPTIONS overrides the one in scripts/buildutils.sh
	./configure --prefix=./debian/tmp/usr/ \
		--gprbuild-params="$(BUILDER_OPTIONS)"

override_dh_auto_install:
	dh_auto_install
	rm -f $(LIBDIR)/lib$(PN).so

	chmod 644 $(LIBDIR)/*
	chmod 644 debian/tmp/usr/lib/$(PN)/relocatable

	mv $(LIBDIR)/*.so* debian/tmp/usr/lib
	mv debian/tmp/usr/lib/$(PN)/static/lib$(PN).a debian/tmp/usr/lib

	# Create symlinks for library and helper
	ln -sf lib$(PN).so.`basename debian/tmp/usr/lib/lib$(PN).so.*.* | sed -e 's/lib$(PN).so.//'` debian/tmp/usr/lib/lib$(PN).so

	mkdir -p debian/tmp/usr/share/ada/adainclude
	mv debian/tmp/usr/src/$(PN) debian/tmp/usr/share/ada/adainclude
	rm -f debian/tmp/usr/share/ada/adainclude/$(PN).ads-e
	chmod -R 444 debian/tmp/usr/share/ada/adainclude/$(PN)/*

	cp debian/misc/$(PN).gpr debian/tmp/usr/share/ada/adainclude
	cp debian/misc/$(PN)-examples.gpr samples

	mkdir -p debian/tmp/usr/lib/ada/adalib/$(PN)
	mv $(LIBDIR)/*.ali debian/tmp/usr/lib/ada/adalib/$(PN)
	chmod -R 444 debian/tmp/usr/lib/ada/adalib/$(PN)/*

	rm -rf debian/tmp/usr/lib/$(PN)
	rm -rf debian/tmp/usr/lib/gnat

override_dh_install:
	dh_install --all --fail-missing

override_dh_compress:
	dh_compress -X.ads -X.adb -XMakefile

override_dh_strip:
	dh_strip --dbg-package=lib$(PN)-dbg
